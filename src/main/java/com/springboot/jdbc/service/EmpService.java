package com.springboot.jdbc.service;

import com.springboot.jdbc.bean.Employee;
import com.springboot.jdbc.mapper1.EmpMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class EmpService {

    @Autowired
    EmpMapper empMapper;

    @Cacheable(cacheNames = "emp",key = "#id")
    public Employee getEmp(Integer id){
        System.out.println("查询"+id);
        Employee emp = empMapper.getEmpById(id);
        return emp;
    }

}
