package com.springboot.jdbc.mapper1;

import com.springboot.jdbc.bean.Department;
import org.apache.ibatis.annotations.*;


public interface DeptMapper {
    public Department getDeptById(Integer id);

    public Boolean insertDept(Department department);

    @Update("UPDATE department SET departmentName = #{departmentName}' WHERE id = #{id}")
    public Boolean updateDept(Department department);

    @Delete("DELETE from department where id = #{id}")
    public Boolean deleteDept(Integer id);

}
