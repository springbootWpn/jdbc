package com.springboot.jdbc.mapper1;

import com.springboot.jdbc.bean.Employee;

import java.util.List;

public interface EmpMapper {
    public Employee getEmpById(Integer id);
}
