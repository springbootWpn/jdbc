package com.springboot.jdbc.mapper2;

import com.springboot.jdbc.bean.Employee;

import java.util.List;
import java.util.Map;

public interface Emp2Mapper {

    public List<Map<String,Object>> getEmpById(Integer id);
}
