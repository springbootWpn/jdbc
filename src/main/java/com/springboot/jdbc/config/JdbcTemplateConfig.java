package com.springboot.jdbc.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class JdbcTemplateConfig {

    @Bean
    //如果DatasourceConfig用Bean(name="onetemplate")进行改变，这里的first也要改变。我是这么想的。
    public JdbcTemplate onetemplate(@Qualifier("dataSourceOne") DataSource dataSource){//@Qualifier("first")指定第一个数据源。first为数据源对应的方法名。不指定数据源，由于有两个，因此会报错
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public JdbcTemplate twotemplate(@Qualifier("dataSourceTwo") DataSource dataSource){//@Qualifier("first")指定第二个数据源。first为数据源对应的方法名
        return new JdbcTemplate(dataSource);
    }
}
