package com.springboot.jdbc.controller;

import com.springboot.jdbc.bean.Department;
import com.springboot.jdbc.mapper1.DeptMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@RestController
public class DeptController {

    @Autowired
    RedisCacheManager objcacheManager;

    @Autowired
    DeptMapper deptMapper;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    StringRedisTemplate stringRedisTemplate;

    @Resource(name = "onetemplate")
    JdbcTemplate jdbcTemplate;

    @GetMapping("/dept/{id}")
    @Cacheable(cacheNames = "dept")
    public Department getDept(@PathVariable("id") Integer id){
        Department deptById = deptMapper.getDeptById(id);
//        Cache deptcache = objcacheManager.getCache("dept");
//        deptcache.put("dept:1",deptById);
//        Department department = deptcache.get("dept:1", Department.class);
//        System.out.println(department);
        return deptById;
    }


    @PostMapping("/insertdept")
    public Boolean getDept(Department department){
        Boolean aBoolean = deptMapper.insertDept(department);
        if (aBoolean){
            Department deptById = deptMapper.getDeptById(department.getId());
            Cache deptcache = objcacheManager.getCache("dept");
            deptcache.put(department.getId(),deptById);
        }
//        Department department = deptcache.get("dept:1", Department.class);
//        System.out.println(department);
        return aBoolean;
    }

    @GetMapping("/testredis")
    public void miaosha(){
//       System.out.println(stringRedisTemplate.opsForValue().get("product"));
        //利用redis的setnx来实现分布式锁
        synchronized (this){
            //设置过期时间后创建的key可能被正在执行的线程删除掉，会造成创建的key和删除的key不是由同一个线程所创建的，利用一个随机值来作为判断标识确保删除的是当前线程所创建的key
            UUID uuid = UUID.randomUUID();
            //如果发生异常，确保finalley将key删除掉
            try {
                stringRedisTemplate.opsForValue().setIfAbsent("lock", uuid.toString(),10, TimeUnit.SECONDS);
                if (stringRedisTemplate.hasKey("lock")){
                    System.out.println("线程正在操作，请等待！");
                }
                Integer product = Integer.parseInt(stringRedisTemplate.opsForValue().get("product").toString());
                if (product > 0) {
                    stringRedisTemplate.opsForValue().decrement("product");
                    System.out.println("库存剩余" + stringRedisTemplate.opsForValue().get("product"));
                } else {
                    System.out.println("库存不足！");
                }
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                //删除锁的时候必须是原子性操作，不然很可能发生时间失效了但是没删除
                if (uuid.toString().equals(stringRedisTemplate.opsForValue().get("lock").toString())){
                    stringRedisTemplate.delete("lock");
                }
            }
        }
    }
}
