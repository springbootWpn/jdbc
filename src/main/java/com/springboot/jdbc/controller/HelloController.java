package com.springboot.jdbc.controller;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.springboot.jdbc.bean.Employee;
import com.springboot.jdbc.mapper2.Emp2Mapper;
import com.springboot.jdbc.mapper1.EmpMapper;
import com.springboot.jdbc.service.EmpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Controller
public class HelloController {

    @Resource(name = "twotemplate")
    private JdbcTemplate twotemplate;


    @Autowired
    EmpService empService;

    @Autowired
    Emp2Mapper emp2Mapper;

    @Autowired
    RedisCacheManager objcacheManager;


    @ResponseBody
    @GetMapping("/getemp/{id}")
    public Employee getEmp(@PathVariable("id") Integer id){
        Employee emp = empService.getEmp(id);
        return emp;
    }




    @ResponseBody
    @GetMapping("/getemp2/{id}")
    public List<Map<String,Object>> getemp2(@PathVariable("id") Integer id){
//        Map<String, Object> stringObjectMap = twotemplate.queryForMap("getEmpById " ,id);
//        System.out.println(stringObjectMap.toString());
        System.out.println(twotemplate.getDataSource());
        Page<Employee> page = PageHelper.startPage(1, 2);
        List<Map<String,Object>> emp = emp2Mapper.getEmpById(id);
        System.out.println(page.getTotal());
        return emp;
    }

}
