package com.springboot.jdbc;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.springboot.jdbc.bean.Employee;
import com.springboot.jdbc.service.EmpService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.connection.Connection;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;


import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;

@RunWith(SpringRunner.class)
@SpringBootTest
class MyjdbcApplicationTests {
	@Autowired
	EmpService empService;

	@Autowired
	RedisTemplate redisTemplate;

	@Autowired
	RedisTemplate<Object, Employee> employeeRedisTemplate;

	@Autowired
	StringRedisTemplate stringRedisTemplate;

	@Autowired
	RabbitTemplate rabbitTemplate;


	@Test
	void pulisher() {
		HashMap<String, Object> stringObjectHashMap = new HashMap<>();
		stringObjectHashMap.put("test","测试direct模式");
		stringObjectHashMap.put("lst", Arrays.asList("helloword",123,true));
		rabbitTemplate.convertAndSend("exchange.direct","atguigu",stringObjectHashMap);
	}

	@Test
	void receive() {
		//获取队列中某个队列的消息个数
		ConnectionFactory connectionFactory = rabbitTemplate.getConnectionFactory();
		Connection connection = connectionFactory.createConnection();
		Channel channel = connection.createChannel(false);
		AMQP.Queue.DeclareOk atguigu1 = null;
		try {
//			channel.exchangeDeclare("exchange.direct", "direct");
			atguigu1 = channel.queueDeclarePassive("atguigu.news");
			int messageCount = atguigu1.getMessageCount();
			System.out.println(messageCount);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				connection.close();
				channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (TimeoutException e) {
				e.printStackTrace();
			}
		}
//		Object atguigu = rabbitTemplate.receiveAndConvert("atguigu");
//		System.out.println(atguigu.getClass());
//		System.out.println(atguigu);
	}

	@Test
	void contextLoads() {
		ValueOperations<String, String> stringops = stringRedisTemplate.opsForValue();
//		stringops.set("test","wpn");
		String test = stringops.get("test");
		System.out.println(test);
//		Employee emp = empService.getEmp(1);
//		employeeRedisTemplate.opsForValue().set("emp-01",emp);
//		System.out.println(employeeRedisTemplate.opsForValue().get("emp-01").toString());
	}


	@Test
	void miaosha() {

	}

}
